;; Copyright 2020 Walter Lewis

;; This file is part of Scheme-eff.

;; Scheme-eff is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option)
;; any later version.

;; Scheme-eff is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Scheme-eff.  If not, see <https://www.gnu.org/licenses/>.

(define-module (eff error))
(use-modules (eff) (srfi srfi-9))

(define-record-type err-req
  (req-err str)
  err-req?
  (str err-req-str))

(define (err str . args)
  (let ((r (req-err (apply format #f str args))))
    (eff r (const r))))

(define try
  (handler k identity 
    (err-req? (lambda (r) (format #f "Error: ~a\n" (err-req-str r))))
    (extend-eff x (try (k x)))))

(export err try)
