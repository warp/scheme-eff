;; Copyright 2020 Walter Lewis

;; This file is part of Scheme-eff.

;; Scheme-eff is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option)
;; any later version.

;; Scheme-eff is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Scheme-eff.  If not, see <https://www.gnu.org/licenses/>.

(define-module (eff nondet))
(use-modules (eff) (eff error) (srfi srfi-9))

(define-record-type fail-req
  (req-fail)
  fail-req?)

(define-record-type choice-req
  (req-choice left right)
  choice-req?
  (left choice-req-left)
  (right choice-req-right))

(define fail (eff (req-fail) identity))

(define (choice left right)
  ((bind-eff
    (lambda (x)
      ((bind-eff
        (lambda (y) (eff (req-choice x y) identity)))
       right)))
   left))

(define any
  (handler k identity
    (fail-req? (const (any (k '()))))
    (choice-req?
     (lambda (r)
       (let ((left (choice-req-left r))
             (right (choice-req-right r)))
         (if (and (list? left) (list? right))
             (any (k (append left right)))
             (err "In `any': attempt to append non-lists")))))
    (extend-eff x (any (k x)))))

(export fail choice any)
