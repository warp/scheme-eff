;; Copyright 2020 Walter Lewis

;; This file is part of Scheme-eff.

;; Scheme-eff is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option)
;; any later version.

;; Scheme-eff is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Scheme-eff.  If not, see <https://www.gnu.org/licenses/>.

(define-module (eff writer))
(use-modules (eff) (srfi srfi-9))

(define-record-type tell-req
  (req-tell msg)
  tell-req?
  (msg tell-req-msg))

(define tell (bind-eff (lambda (x) (eff (req-tell x) identity))))

(define (writer msg ++)
  (handler k (lambda (x) (cons msg x))
    (tell-req?
     (lambda (r)
       ((writer (++ msg (tell-req-msg r)) ++)
        (k '()))))
    (extend-eff x ((writer msg ++) (k x)))))

(export tell writer)
