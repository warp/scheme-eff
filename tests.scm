;; Copyright 2020 Walter Lewis

;; This file is part of Scheme-eff.

;; Scheme-eff is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option)
;; any later version.

;; Scheme-eff is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Scheme-eff.  If not, see <https://www.gnu.org/licenses/>.

(use-modules
 (eff)
 (eff error)
 (eff reader)
 (eff state)
 (eff writer)
 (eff trace)
 (eff nondet))

(define (test env st)
  (try
   ((reader env)
    ((state st)
     ((writer "begin test: " string-append)
      (trace!
       (begin-eff
        (tell "getting initial state, ")
        (into s1 get)
        (tell "adding with env, ")
        (if (or (not (number? s1))
                (not (number? env)))
            (err "not a number")
            (let ((s2 (+ s1 env)))
              (begin-eff
               (tell (format #f "putting ~a, " s2))
               (put s2)
               (tell (format #f "returning ~a." env))
               env))))))))))

(define (test2)
  (any
   ((state fail)
    (trace!
     (begin-eff
      (into s1 get)
      (put (choice s1 '(1 2 3)))
      (into s2 get)
      (put (choice s2 '(4 5)))
      get)))))
