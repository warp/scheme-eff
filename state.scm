;; Copyright 2020 Walter Lewis

;; This file is part of Scheme-eff.

;; Scheme-eff is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option)
;; any later version.

;; Scheme-eff is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Scheme-eff.  If not, see <https://www.gnu.org/licenses/>.

(define-module (eff state))
(use-modules (eff) (srfi srfi-9))

(define-record-type get-req
  (req-get)
  get-req?)

(define-record-type put-req
  (req-put st)
  put-req?
  (st put-req-st))

(define get (eff (req-get) identity))

(define put (bind-eff (lambda (x) (eff (req-put x) identity))))

(define (state st)
  (handler k identity
    (get-req? (const ((state st) (k st))))
    (put-req?
     (lambda (r)
       ((state (put-req-st r))
        (k '()))))
    (extend-eff x ((state st) (k x)))))

(export get put state)
