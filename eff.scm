;; Copyright 2020 Walter Lewis

;; This file is part of Scheme-eff.

;; Scheme-eff is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option)
;; any later version.

;; Scheme-eff is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Scheme-eff.  If not, see <https://www.gnu.org/licenses/>.

(define-module (eff))

(define eff-tag '(eff))
(define (eff r k) (cons eff-tag (cons r k)))
(define (eff? x) (and (pair? x) (eq? (car x) eff-tag)))
(define eff-req cadr)
(define eff-cont cddr)

(define-syntax-rule (handler k return (pred? handle) ... finally)
  (lambda (x)
    (if (not (eff? x)) (return x)
        (let ((r (eff-req x)) (k (eff-cont x)))
          (or (and (pred? r) (handle r)) ...
              (finally r))))))

(define-syntax-rule (extend-eff x expr)
  (lambda (r)
    (eff r (lambda (x) expr))))

(define (bind-eff k)
  (handler k0 k
    (extend-eff x ((bind-eff k) (k0 x)))))

(define-syntax begin-eff
  (syntax-rules (into)
    ((begin-eff e) e)
    ((begin-eff (into x e0) e ...)
     ((bind-eff (λ (x) (begin-eff e ...))) e0))
    ((begin-eff e0 e ...)
     ((bind-eff (const (begin-eff e ...))) e0))))

(export eff handler extend-eff bind-eff begin-eff)
